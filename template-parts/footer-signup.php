<?php
    /**
     * Mailchimp Footer Signup
     *
     */
?>

<div class="col-md-3 footer-ns">
    <h4 class="mb-4">Newsletter Signup</h4>
    <form action="https://thecraftrevolt.us17.list-manage.com/subscribe/post?u=4e3a46583f7a4c18e57c40d05&id=633de34566"
          method="post" id="mc-embedded-subscribe-form" name="mc-embedded-subscribe-form" class="validate"
          target="_blank" novalidate>
        <div class="input-group mb-3">
            <input type="text" class="form-control" type="email" value="" name="EMAIL" id="mce-EMAIL"
                   placeholder="Email" required placeholder="Recipient's username"
                   aria-label="Recipient's username" aria-describedby="basic-addon2">
        </div>
        <div class="input-group  mb-3">
            <input type="text" value="" name="MMERGE5" class="form-control" placeholder="ZIP Code" id="mce-MMERGE5">
        </div>
        <div class="input-group">
            <button class="btn btn-outline-secondary" type="submit" value="Subscribe" name="subscribe"
                    id="mc-embedded-subscribe">Subscribe
            </button>
        </div>
        <div style="position: absolute; left: -5000px;" aria-hidden="true">
            <input type="text" name="b_4e3a46583f7a4c18e57c40d05_633de34566" tabindex="-1" value="">
        </div>
    </form>
</div>
