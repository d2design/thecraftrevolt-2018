<?php
    /**
     * Hero Widget for Front Page
     *
     * @package WordPress
     * @subpackage Twenty_Seventeen
     * @since 1.0
     * @version 1.0
     */

?>

<main role="main">
    <div class="jumbotron jumbotron-fluid">
        <?php if ( is_front_page() ) {
            the_custom_header_markup();

        } ?>
        <div class="hero-widget">
            <?php dynamic_sidebar( 'hero' ); ?>
        </div>
    </div>
</main>
<!-- Banner End -->