<?php
/**
 * Displays Contact
 *
 * @package WordPress
 * @subpackage Twenty_Seventeen
 * @since 1.0
 * @version 1.0
 */

?>
<!-- Assemble section banner Start -->
<section class="assemble-section faqs-section" id="faqs">
	<div class="container">
		<div class="row">
			<div class="col-sm-12">	
				<?php $the_query = new WP_Query( 'page_id=166' ); ?>
				<?php while ($the_query -> have_posts()) : $the_query -> the_post();  ?>
					<h2>FAQs</h2>
				    <?php the_content(); ?>

				<?php endwhile; wp_reset_postdata(); ?>
			</div>
		</div>
	</div>
</section>
<!-- Assemble section  End -->
