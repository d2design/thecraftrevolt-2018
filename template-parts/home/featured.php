<?php
    /**
     * Displays Pages that are assigned to featured category.
     *
     * @package WordPress
     * @subpackage Twenty_Seventeen
     * @since 1.0
     * @version 1.0
     */

    $args = [
        'category_name' => 'featured',
        'orderby' => 'menu_order'
    ];

    // the query
    $the_query = new WP_Query( $args ); ?>

    <?php if ( $the_query->have_posts() ) :  ?>
    <?php while ( $the_query->have_posts() ) : $the_query->the_post(); ?>
        <section class="mb-5">
            <div class="container">
                <div class="row">
                    <div class="col-12">
                        <h2><?php the_title(); ?></h2>
                    </div>
                </div>
                <div class="row mb-2">
                    <div class="col-12 mb-2 featured-sections">
                        <?php the_post_thumbnail(); ?>
                    </div>
                </div>
                <div class="row">
                    <div class="col-12 mb-5">
                        <h5><?php the_excerpt(); ?></h5>
                    </div>
                </div>
            </div>
        </section>
    <?php endwhile; ?>

    <?php wp_reset_postdata(); ?>

<?php else : ?>
    <p><?php esc_html_e( 'Sorry, no posts matched your criteria.' ); ?></p>
<?php endif; ?>
