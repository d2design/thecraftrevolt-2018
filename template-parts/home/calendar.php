<?php
    /**
     * Displays Homepage Calendar
     *
     * @package WordPress
     * @subpackage Twenty_Seventeen
     * @since 1.0
     * @version 1.0
     */

?>
<!-- Assemble section banner Start -->
<section class="assemble-section classes-section" id="calendar">
    <div class="container">
        <div class="row">
            <div class="col-sm-12 mb-5">
                <?php
                    echo do_shortcode('[add_eventon_list cal_id="Homepage" number_of_months="1" event_count="100" hide_past="yes" hide_past_by="ss" show_et_ft_img="yes" ux_val="1" ]');
                ?>
            </div>
        </div>
    </div>
</section>
<!-- Assemble section  End -->
