<?php
    /**
     * Displays Homepage Calendar
     *
     * @package WordPress
     * @subpackage Twenty_Seventeen
     * @since 1.0
     * @version 1.0
     */
?>


<section>
    <div class="container">
        <div class="row">
            <?php
                $terms = get_terms( [
                    'taxonomy' => 'event_type',
                    'parent'   => 0,
                ] );

                if ( !empty( $terms ) && !is_wp_error( $terms ) ) {
                    foreach ( $terms as $term ) {
                        $html = '<div class="col-lg-4 col-sm-6">';
                        $html .= '<div class="card mb-5" style="width: 100%">';
                        $html .= '<a href="' . get_term_link( $term->slug, 'event_type' ) . '">';
                        $html .= '<img class="card-img-top" src="' . z_taxonomy_image_url( $term->term_id, 'x-small' ) . '" alt="' . $term->name . '">';
                        $html .= '</a>';
                        $html .= '<div class="card-body">';
                        $html .= '<a href="' . get_term_link( $term->slug, 'event_type' ) . '">';
                        $html .= '<h5 class="card-title">' . $term->name . '</h5>';
                        $html .= '</a>';
                        $html .= '<p class="card-text">' . $term->description . '</p>';
                        $html .= '</div>';
                        $html .= '</div>';
                        $html .= '</div>';

                        echo $html;
                    }

                }
            ?>
        </div>
    </div>
</section>
