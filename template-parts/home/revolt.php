<?php
/**
 * Displays Revolt
 *
 * @package WordPress
 * @subpackage Twenty_Seventeen
 * @since 1.0
 * @version 1.0
 */

?>
<!-- Assemble section banner Start -->
<section class="assemble-section text-center" id="revolt" style="border-bottom: 1px solid #ddd;">
	<div class="container">
		<div class="row">
			<div class="col-sm-12">
				<!-- <h2>The Craft Revolt: Assemble Here</h2>
				<p class="highlight-txt"><a href="<?php echo home_url('/#revolt') ?>" class="highlight-txt">Crafting just got serious.</a></p>
				<p> A thing is not just a thing, but it is what made it. It is wood, glass, fabric, yarn, paint, metal, hope, symbolism,
					time, imagination, care, love... Once you start to make, instead of absorb, you will feel yourself change. You are
					more than your TV shows, your politics, your brands, your favorite foods...You are a maker. You are Brave. Proud. Creative.
					Artistic. Valued. Strong. Exercise your right to create.</p> -->
				
				<?php $the_query = new WP_Query( 'page_id=160' ); ?>
				<?php while ($the_query -> have_posts()) : $the_query -> the_post();  ?>

				    <?php the_content(); ?>

				<?php endwhile; wp_reset_postdata(); ?>
			</div>
		</div>
	</div>
</section>
<!-- Assemble section  End -->
