<?php
/**
 * Displays Contact
 *
 * @package WordPress
 * @subpackage Twenty_Seventeen
 * @since 1.0
 * @version 1.0
 */

?>
<!-- Assemble section banner Start -->
<section class="assemble-section text-center" id="contactUs">
	<div class="container">
		<div class="row">
			<div class="col-sm-12">	
				<h2>Contact Us</h2>			
				<?php echo do_shortcode(' [contact-form-7 id="187" title="Contact form 1"] ') ?>
			</div>
		</div>
	</div>
</section>
<!-- Assemble section  End -->