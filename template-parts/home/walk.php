<?php
/**
 * Displays Walk
 *
 * @package WordPress
 * @subpackage Twenty_Seventeen
 * @since 1.0
 * @version 1.0
 */

?>
<?php
  $event_terms = get_terms(
		'event_type',
		array(
			'orderby'=>'name',
			'hide_empty'=>false
		)
	);
  if(!empty($event_terms) && !is_wp_error($event_terms)){
?>
<!-- WalkIn Start -->
<section class="walk-in">
	<h2>Walk In</h2>
	<div class="container">
		<div class="row">
			<?php 
                foreach($event_terms as $event_term){
              ?>
				<div class="col-sm-6 col-md-3">
					<div class="walk-one text-center">
						<div class="circle-img-walk d-flex justify-content-center">
							<img src="<?php echo get_template_directory_uri(); ?>/images/walkin-img-01.png" alt="" class="align-self-center">
						</div>
						<h4><?php echo $event_term->name;?></h4>
						<a href="<?php echo get_term_link($event_term); ?>">View Details</a>
					</div>
				</div>
			<?php } ?>
		</div>
		<div id="pr-party"></div>
	</div>
</section>
<!-- WalkIn End -->
<?php } ?>