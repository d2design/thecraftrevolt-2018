<?php
/**
 * Displays Calender
 *
 * @package WordPress
 * @subpackage Twenty_Seventeen
 * @since 1.0
 * @version 1.0
 */

?>
<!-- Schedule Section End -->
<div class="schedule-section">
  <div class="container">
    <p>&nbsp;</p>
	<h3>Classes</h3>
	<!--<div id='calendar'></div>-->
  <?php //echo do_shortcode("[add_eventon_fc cal_id='Fulcal1' jumper='yes' mo1st='yes' heat='yes' show_et_ft_img='yes' evc_open='yes' hover='numname']");?>
  <div class="row">  
        <?php

          $custom_query_args = array(
            'post_type' => 'ajde_events', 
            'posts_per_page' => 6,
            'paged' => $paged,
            'post_status' => 'publish',
            'ignore_sticky_posts' => true,
            'order' => 'ASC',
            'orderby' => 'evcal_srow',
            'meta_query' => array(
                array(
                    'key'     => 'evcal_srow',
                    'value'   => strtotime(date("m/d/Y")),
                    'compare' => '>=',
                ),
            ),
          );
        $custom_query = new WP_Query( $custom_query_args );

        if ( $custom_query->have_posts() ) :
            while( $custom_query->have_posts() ) : $custom_query->the_post();
          $img_id =get_post_thumbnail_id(get_the_ID());
          $img_src = wp_get_attachment_image_src($img_id,'medium');
          $ProId = get_post_meta(get_the_ID(), 'tx_woocommerce_product_id' );
          $Orgterms = wp_get_object_terms( get_the_ID(), 'event_organizer' );
          $pmv = get_post_meta(get_the_ID());
          // /print_r($pmv);
          $start = (!empty($pmv['evcal_srow'])?$pmv['evcal_srow'][0]:'');
          if(!empty($start)){
              // date and time as separate columns
              $startdate= date( apply_filters('evo_csv_export_dateformat','m/d/Y'), $start);
              $starttime= date( apply_filters('evo_csv_export_timeformat','h:i:A'), $start);
              //$csvRow.= '"'. date($date_time_format, $start) .'",';
            }
          $end = (!empty($pmv['evcal_erow'])?$pmv['evcal_erow'][0]:'');
          if(!empty($end)){
              // date and time as separate columns
              $enddate= date( apply_filters('evo_csv_export_dateformat','m/d/Y'), $end);
              $endtime= date( apply_filters('evo_csv_export_timeformat','h:i:A'), $end);
              //$csvRow.= '"'. date($date_time_format,$end) .'",';
            }
          $Locterms = wp_get_object_terms( get_the_ID(), 'event_location' );
          $Orgterms = wp_get_object_terms( get_the_ID(), 'event_organizer' );
        ?>

        <div class="col-lg-6 col-12">
          <div class="grid-box border mb-4">
            <div class="d-flex flex-wrap">
              <div class="image text-center">
                <img src="<?php echo $img_src[0];?>" alt="" class="img-fluid">
                <div class="view-details d-flex align-items-center">
                    <div class="text-center w-100">
                      <a href="<?php the_permalink(); ?>" class="btn btn-view">View Details</a>
                    </div>
                </div>
              </div>
              <div class="caption px-3 py-1">
                <div class="w-100">
                  <h5 class="font-weight-bold text-truncate pb-0"><a href="<?php the_permalink(); ?>"><?php the_title(); ?></a></h5>
                </div>
                <?php
                if(!empty($Orgterms[0])) {
                ?>
                <div class="w-100 d-flex pb-1">
                  <div class="w-40"><span class="price text-info">$<?php echo $pmv['evotx_price'][0];?> USD</span></div>
                  <div class="w-60 text-truncate">
                    <b>Teacher:</b> <?php echo $Orgterms[0]->name;?>
                  </div>
                </div>
                <?php } ?>
                <div class="w-100 d-flex">
                  <div class="w-40"><i class="fa fa-calendar"></i> <?php echo $startdate;?></div>
                  <div class="w-60"><i class="fa fa-clock-o"></i> <?php echo $starttime;?> - <?php echo $endtime;?></div>
                </div>
                <!-- <div class="w-100 d-flex" style="position: relative;">
                <a class="grid-view-link">View All</a>
              </div> -->
                <?php
                $evcal_repeat = (!empty($pmv["evcal_repeat"]) )? $pmv["evcal_repeat"][0]:null;
                        $count =0;
                        if(!empty($pmv['repeat_intervals'])){
                ?>
                <div class="w-100 d-flex" style="position: relative;">
                  <!-- hover div -->
                  <a class="grid-view-link">View All</a>
                  <div class="grid-view-all-dates">
                    <table class="table table-bordered">
                      <?php
                                $repeat_times = (unserialize($pmv['repeat_intervals'][0]));
                                //print_r($repeat_times);

                                // datre format sting to display for repeats
                                $date_format_string = $evcal_date_format[1].' '.( $evcal_date_format[2]? 'G:i':'h:ia');
                                
                                foreach($repeat_times as $rt){
                                  $startUNIX = (int)$rt[0];
                                  $endUNIX = (int)$rt[1];
                        ?>
                      <tr>
                          <td><i class="fa fa-calendar"></i>  <?php echo date('m-d-Y',$startUNIX);?></td> 
                          <td><i class="fa fa-clock-o"></i>  <?php echo date($date_format_string,$startUNIX);?> - <?php echo date($date_format_string,$endUNIX);?></td>
                        </tr>
                        <?php
                        $count++;
                          }
                        ?>
                    </table>
                  </div>
                  <!-- end hover div -->

                </div>
                <?php } ?>
                <div class="w-100 d-flex flex-wrap pt-1">
                  <div class="w-100"><b>Location:</b></div>
                  <div class="w-100 text-truncate"><?php echo $Locterms[0]->name;?></div>
                </div>
              </div>
            </div>            
          </div>
        </div>
        


        <?php
          endwhile;
        ?>

        <?php
              wp_reset_postdata();
          else:
              echo '<div class="col-12 d-flex justify-content-center">'.__('Sorry, no posts matched your criteria.').'</div>';
          endif;
          ?>

      <div id="walk"></div>
      </div>
    </div>
</div>
<!-- Schedule Section End -->

<!--<script type="text/javascript">
	jQuery('#calendar').fullCalendar({
      header: {
        left: 'prev,next today',
        center: 'title',
        right: 'month,agendaWeek,agendaDay,listWeek'
      },
      defaultDate: '2017-09-12',
      navLinks: true, // can click day/week names to navigate views
      editable: true,
      eventLimit: true, // allow "more" link when too many events
      events: [
        {
          title: 'All Day Event',
          start: '2017-09-01',
        },
        {
          title: 'Long Event',
          start: '2017-09-07',
          end: '2017-09-10'
        },
        {
          id: 999,
          title: 'Repeating Event',
          start: '2017-09-09T16:00:00'
        },
        {
          id: 999,
          title: 'Repeating Event',
          start: '2017-09-16T16:00:00'
        },
        {
          title: 'Conference',
          start: '2017-09-11',
          end: '2017-09-13'
        },
        {
          title: 'Meeting',
          start: '2017-09-12T10:30:00',
          end: '2017-09-12T12:30:00'
        },
        {
          title: 'Lunch',
          start: '2017-09-12T12:00:00'
        },
        {
          title: 'Meeting',
          start: '2017-09-12T14:30:00'
        },
        {
          title: 'Happy Hour',
          start: '2017-09-12T17:30:00'
        },
        {
          title: 'Dinner',
          start: '2017-09-12T20:00:00'
        },
        {
          title: 'Birthday Party',
          start: '2017-09-13T07:00:00'
        },
        {
          title: 'Click for Google',
          url: 'http://google.com/',
          start: '2017-09-28'
        }
      ]
    });
    
</script>-->