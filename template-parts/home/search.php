<?php
/**
 * Search bar on Events pages
 *
 * @package WordPress
 * @subpackage Twenty_Seventeen
 * @since 1.0
 * @version 1.0
 */
?>
<link rel="stylesheet" href="//code.jquery.com/ui/1.12.1/themes/base/jquery-ui.css">
<link rel="stylesheet" href="/resources/demos/style.css">
<script src="https://code.jquery.com/ui/1.12.1/jquery-ui.js"></script>
<form method="get" action="">
	<div class="row">
		<div class="col-md-12">
		  <div class="search-filter-section">
		    <h2>Classes</h2>
		    <div class="from-group">
		      <input type="text" name="keyword" class="form-control search" placeholder="Search by Class Name, Subject, Teacher">
		      <?php
		      $event_terms = get_terms(
					'event_type',
					array(
						'orderby'=>'name',
						'hide_empty'=>false
					)
				);
		      if(!empty($event_terms) && !is_wp_error($event_terms)){
		      ?>
		      <select name="cat_id" class="form-control category">
		      	<option>- Select -</option>
		      	<?php foreach($event_terms as $event_term){ ?>
		        <option value="<?php echo $event_term->term_id;?>"><?php echo $event_term->name;?></option>
		        <?php } ?>
		      </select>
		      <?php } ?>
		      <input type="text" name="date" id="datepicker" class="form-control date" placeholder="Date">
		      <input type="submit" value="Search" name="submit" class="btn btn-view">
		      <ul>
		        <li><a href="<?php echo site_url()?>/event-list"><i class="fa fa-list"></i></a></li>
		        <li><a href="<?php echo site_url()?>/event-grid"><i class="fa fa-th-large"></i></a></li>
		        <li><a href="<?php echo site_url()?>/calendar"><i class="fa fa-calendar"></i></a></li>
		      </ul>
		    </div>
		  </div> 
		</div>
	</div>
</form>
<script>
  jQuery( function() {
    jQuery( "#datepicker" ).datepicker();
  } );
  </script>
