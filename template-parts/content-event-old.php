<?php
/**
 * The template part for displaying single posts
 *
 * @package WordPress
 * @subpackage Twenty_Sixteen
 * @since Twenty Sixteen 1.0
 */
global $product;
$pmv = get_post_meta(get_the_ID());
          //print_r($pmv);

$product = wc_get_product( $pmv['tx_woocommerce_product_id'][0] );

//echo $product->get_title();
?>


<article id="post-<?php the_ID(); ?>" <?php post_class(); ?>>
	<div class="row">
	    <div class="col-12 d-flex flex-wrap pb-1">
	      <div class="image mb-3">
	        <?php twentysixteen_post_thumbnail(); ?>
	      </div>
	      <div class="caption px-3">
	        <h4 class="mb-4"><?php the_title( '', '' ); ?> </h4>
			<form action="<?php echo site_url();?>/cart" method ="get">
				<input type="hidden" name="add-to-cart" value="<?php echo $pmv['tx_woocommerce_product_id'][0];?>">
					<div class="d-flex pb-4 w-100">
						<div class="w-50">
							<div class="input-group">
							  <span class="input-group-addon">Qty</span>
							  <input type="text" name="quantity" min="1" max="10" class="form-control">
							</div>
						</div>
						<div class="w-50 px-4">
							<input type="submit" value="Book Now" class="btn btn-view text-uppercase">
						</div>
					</div>
			</form>
	        <div class="d-flex pb-2 w-100">
	          <div class="pr-lg-5 pr-2">
	            <div class="d-flex">
	              <div class="pr-lg-4 pr-1"><i class="fa fa-calendar"></i>  9-20-2017</div>
	              <div class=""><i class="fa fa-clock-o"></i>  5:00 PM - 6:30 PM</div>
	            </div>
	          </div>
	          <div class="pl-lg-5 pr-2">
	            <div class="d-flex">
	              <div class="pr-lg-4 pr-1"><i class="fa fa-calendar"></i>  9-20-2017</div>
	              <div class=""><i class="fa fa-clock-o"></i>  5:00 PM - 6:30 PM</div>
	            </div>
	          </div>
	        </div>
	        <!-- <div class="d-flex pb-2 w-100">
	          <div class="pr-lg-5 pr-2">
	            <div class="d-flex">
	              <div class="pr-lg-4 pr-1"><i class="fa fa-calendar"></i>  9-20-2017</div>
	              <div class=""><i class="fa fa-clock-o"></i>  5:00 PM - 6:30 PM</div>
	            </div>
	          </div>
	          <div class="pl-lg-5 pr-2">
	            <div class="d-flex">
	              <div class="pr-lg-4 pr-1"><i class="fa fa-calendar"></i>  9-20-2017</div>
	              <div class=""><i class="fa fa-clock-o"></i>  5:00 PM - 6:30 PM</div>
	            </div>
	          </div>
	        </div> -->

	        <div class="d-flex bottom-set flex-wrap">
		      <div class="w-100">
		        <div class="row">
		          <div class="col-sm-12">
		            <div class="price-new py-3">$25 USD/person</div>
		            <div class="d-flex pb-3">
		            	<div class="mr-5">
		            		<b>Subject: </b> Electronics
		            	</div>
		            	<div class="">
		            		<b>Teacher: </b> Alber LB John
		            	</div>		              
		            </div>
		            <div class="w-100 pb-3"><b>Location: </b> Fashin Stree Second Flour Las Vegas Chicago</div>
		          </div>
		          
		        </div>
		      </div>
		    </div>
	      </div>

	    </div>

	    <div class="d-flex flex-wrap description col-12">
          <div class="w-100"><b>Description: </b></div>
          <?php the_content(); ?>
        </div>

	    

	    <div class="col-12">
	    	<?php
				wp_link_pages( array(
					'before'      => '<div class="page-links"><span class="page-links-title">' . __( 'Pages:', 'twentysixteen' ) . '</span>',
					'after'       => '</div>',
					'link_before' => '<span>',
					'link_after'  => '</span>',
					'pagelink'    => '<span class="screen-reader-text">' . __( 'Page', 'twentysixteen' ) . ' </span>%',
					'separator'   => '<span class="screen-reader-text">, </span>',
				) );

				if ( '' !== get_the_author_meta( 'description' ) ) {
					get_template_part( 'template-parts/biography' );
				}
			?>
	    </div>

	</div>

	<div class="row">
		<div class="col-12">
			<footer class="entry-footer">
				<?php twentysixteen_entry_meta(); ?>
				<?php
					edit_post_link(
						sprintf(
							/* translators: %s: Name of current post */
							__( 'Edit<span class="screen-reader-text"> "%s"</span>', 'twentysixteen' ),
							get_the_title()
						),
						'<span class="edit-link">',
						'</span>'
					);
				?>
			</footer><!-- .entry-footer -->
		</div>
	</div>


	<?php //twentysixteen_excerpt(); ?>

	
</article><!-- #post-## -->
