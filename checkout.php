<?php
    /* Template Name: Checkout */

    get_header();
?>
<div id="primary" class="content-area checkout-page pt-3 pb-5">
    <main id="main" class="site-main" role="main">
        <div class="container">
            <div class="row">
                <div class="col-12">
                    <?php
                        // Start the loop.
                        while ( have_posts() ) : the_post();

                            // Include the page content template.
                            get_template_part( 'template-parts/content', 'page' );

                            // End of the loop.
                        endwhile;
                    ?>
                </div>
            </div>
        </div>
    </main><!-- .site-main -->

</div><!-- .content-area -->

<?php get_footer(); ?>
