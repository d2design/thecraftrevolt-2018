<?php
    /**
     * The front page template file
     *
     * If the user has selected a static page for their homepage, this is what will
     * appear.
     * Learn more: https://codex.wordpress.org/Template_Hierarchy
     *
     * @package WordPress
     * @subpackage Twenty_Seventeen
     * @since 1.0
     * @version 1.0
     */
?>

<?php
    global $post;
    //get_header();

    if ( ! post_password_required( $post ) ) {

        get_header();

        get_template_part( 'template-parts/home/hero' );
        get_template_part( 'template-parts/home/classes' );
        get_template_part( 'template-parts/home/calendar' );
        get_template_part( 'template-parts/home/featured' );
        get_template_part( 'template-parts/home/faqs' );

        get_footer();

    } else {
        // we will show password form here
        echo get_the_password_form();
    }
