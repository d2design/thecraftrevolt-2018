<?php
    /**
     * Adds custom classes to the array of body classes.
     *
     * @since Twenty Sixteen 1.0
     *
     * @param array $classes Classes for the body element.
     * @return array (Maybe) filtered body classes.
     */
    function twentysixteen_body_classes( $classes ) {
        // Adds a class of custom-background-image to sites with a custom background image.
        if ( get_background_image() ) {
            $classes[] = 'custom-background-image';
        }

        // Adds a class of group-blog to sites with more than 1 published author.
        if ( is_multi_author() ) {
            $classes[] = 'group-blog';
        }

        // Adds a class of no-sidebar to sites without active sidebar.
        if ( ! is_active_sidebar( 'sidebar-1' ) ) {
            $classes[] = 'no-sidebar';
        }

        // Adds a class of hfeed to non-singular pages.
        if ( ! is_singular() ) {
            $classes[] = 'hfeed';
        }

        return $classes;
    }
    add_filter( 'body_class', 'twentysixteen_body_classes' );