<?php
    /**
     *  Rewrite Rules Support
     */
    add_filter( 'post_type_link', 'my_post_type_link', 10, 3);
    function my_post_type_link($permalink, $post, $leavename) {

        if  ($post->post_type == 'ajde_events') {
            $meta = get_post_meta($post->ID, '_my-post-meta', true);
            if (isset($meta) && !empty($meta))
                $permalink = home_url( "my-friendly-url/" . $meta . "/" . $post->post_name . "/");
        }

        return $permalink;

    }

    add_filter( 'rewrite_rules_array', 'my_rewrite_rules_array');
    function my_rewrite_rules_array($rules) {

        $rules = array('my-friendly-url/([^/]*)/([^/]*)/?$' => 'index.php?post_type=ajde_events&name=$matches[2]&meta=$matches[1]') + $rules;
        return $rules;
    }
