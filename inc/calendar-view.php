<?php
    /**
     * Add calendar view links to eventon calendar header
     *
     * @author Shannon MacMillan
     */
    function bsc_eventon_add_calendar_view_links()
    {

        ?>
        <div class="calendar-views">
            <?php
                wp_nav_menu(array(
                    'theme_location' => 'class_views',
                    'menu_class' => 'navbar-class',
                    'container' => false,
                ));
            ?>
        </div>
        <?php
    }

    add_action('eventon_calendar_header_content', 'bsc_eventon_add_calendar_view_links');

    /**
     * Fix EventON add new event styles.
     */
    function bsc_fix_eventon_styles()
    {
        echo '<style>
	.evomb_header {
		height: 40px;
	}
  </style>';
    }

    add_action('admin_head', 'bsc_fix_eventon_styles');