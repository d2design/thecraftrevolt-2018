<?php ?>
<!-- Event snippet for Purchases conversion page -->
<script>
    gtag('event', 'conversion', {
        'send_to': 'AW-819853499/LsTlCLzVj4EBELvx94YD',
        'value': 1.0,
        'currency': 'USD',
        'transaction_id': ''
    });
</script>
<script>
    window.dataLayer = window.dataLayer || [];

    function gtag() {
        dataLayer.push(arguments);
    }

    gtag('js', new Date());

    gtag('config', 'AW-819853499');
</script>
<!-- Facebook Pixel Code -->
<script>
    !function (f, b, e, v, n, t, s) {
        if (f.fbq) return;
        n = f.fbq = function () {
            n.callMethod ?
                n.callMethod.apply(n, arguments) : n.queue.push(arguments)
        };
        if (!f.fbq) f.fbq = n;
        n.push = n;
        n.loaded = !0;
        n.version = '2.0';
        n.queue = [];
        t = b.createElement(e);
        t.async = !0;
        t.src = v;
        s = b.getElementsByTagName(e)[0];
        s.parentNode.insertBefore(t, s)
    }(window, document, 'script',
        'https://connect.facebook.net/en_US/fbevents.js');
    fbq('init', '389754514769548');
    fbq('track', 'PageView');
</script>
<noscript>
    <img height="1" width="1"
         src="https://www.facebook.com/tr?id=389754514769548&ev=PageView
&noscript=1"/>
</noscript>
<!-- End Facebook Pixel Code -->

<!--  LuckyOrange  Code -->
<script type='text/javascript'>
    window.__lo_site_id = 106600;

    (function () {
        var wa = document.createElement('script');
        wa.type = 'text/javascript';
        wa.async = true;
        wa.src = 'https://d10lpsik1i8c69.cloudfront.net/w.js';
        var s = document.getElementsByTagName('script')[0];
        s.parentNode.insertBefore(wa, s);
    })();
</script>