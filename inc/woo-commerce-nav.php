<?php
    /**
     * Created by PhpStorm.
     * User: daviddiomede
     * Date: 7/8/18
     * Time: 7:53 PM
     */
    /**
     * Place a cart icon with number of items and total cost in the menu bar.
     *
     * Source: http://wordpress.org/plugins/woocommerce-menu-bar-cart/
     */
    add_filter('wp_nav_menu_items','sk_wcmenucart', 10, 2);
    function sk_wcmenucart($menu, $args) {

        // Check if WooCommerce is active and add a new item to a menu assigned to Primary Navigation Menu location
        if ( !in_array( 'woocommerce/woocommerce.php', apply_filters( 'active_plugins', get_option( 'active_plugins' ) ) ) || 'primary' !== $args->theme_location )
            return $menu;

        ob_start();
        global $woocommerce;
        $viewing_cart = __('View your shopping cart', 'your-theme-slug');
        $start_shopping = __('Start shopping', 'your-theme-slug');
        $cart_url = $woocommerce->cart->get_cart_url();
        $shop_page_url = get_permalink( wc_get_page_id( 'shop' ) );
        $cart_contents_count = $woocommerce->cart->cart_contents_count;
        $cart_contents = sprintf(_n('%d item', '%d items', $cart_contents_count, 'your-theme-slug'), $cart_contents_count);
        $cart_total = $woocommerce->cart->get_cart_total();
        // Uncomment the line below to hide nav menu cart item when there are no items in the cart
        // if ( $cart_contents_count > 0 ) {
        if ($cart_contents_count == 0) {
            $menu_item = '</ul><ul class="nav navbar-nav navbar-right"><li><a class="wcmenucart-contents" href="'. $shop_page_url .'" title="'. $start_shopping .'">';
        } else {
            $menu_item = '<li><a class="wcmenucart-contents" href="'. $cart_url .'" title="'. $viewing_cart .'"></ul>';
        }

        $menu_item .= '<i class="fa fa-shopping-cart"></i> ';

        $menu_item .= $cart_contents.' - '. $cart_total;
        $menu_item .= '</a></li>';
        // Uncomment the line below to hide nav menu cart item when there are no items in the cart
        // }
        echo $menu_item;
        $social = ob_get_clean();
        return $menu . $social;

    }