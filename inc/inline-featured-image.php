<?php
    /**
     * Created by PhpStorm.
     * User: daviddiomede
     * Date: 8/5/18
     * Time: 4:26 PM
     */

     /**
     * Add color styling from theme
     */
    function inline_styles_method()
    {
        wp_enqueue_style(
            'main_style',
            get_template_directory_uri() . '/style.css'
        );

        global $post;
        $custom_post_feature = '';
        if ( is_page() || is_single() && has_post_thumbnail() ) {
            $image = [
                'small' => get_the_post_thumbnail_url( $post->ID, 'small' ),
                'medium' => get_the_post_thumbnail_url( $post->ID, 'medium' ),
                'large' => get_the_post_thumbnail_url( $post->ID, 'large' ),
            ];

            $grid_breakpoint = [
                'small' => '@media screen and (min-width: 576px)',
                'medium' => '@media screen and (min-width: 768px)',
                'large' => '@media screen and (min-width: 992px)',
            ];

            if ( !empty( $image ) ) {
                $custom_post_feature = '.custom-post-feature-img {
                        background:url(' . $image[ 'small' ] . '); 
                        background-size: cover; 
                        background-position: center bottom;
                        background-repeat: no-repeat;
                        border: 3px solid white;
                        box-shadow: 2px 2px 4px rgba(0,0,0,.5);
                        height: 12vh;
                    }';

                $custom_post_feature .=
                    $grid_breakpoint[ 'large' ] . '{
                        .custom-post-feature-img {
                            background:url(' . $image[ 'large' ] . '); 
                            background-size: cover; 
                            background-position: center;
                            background-repeat: no-repeat;
                            border: 6px solid white;
                            box-shadow: 2px 2px 4px rgba(0,0,0,.5);
                            height: 35vh; 
                      }
                    }';

                $custom_post_feature .=
                    $grid_breakpoint[ 'medium' ] . '{
                        .custom-post-feature-img {
                            background:url(' . $image[ 'medium' ] . '); 
                            background-size: cover; 
                            background-position: center bottom;
                            background-repeat: no-repeat;
                            border: 6px solid white;
                            box-shadow: 2px 2px 4px rgba(0,0,0,.5);
                            height: 35vh;         
                        }
                     }';
            };
        }
        wp_add_inline_style( 'main_style', $custom_post_feature );

    }

    add_action( 'wp_enqueue_scripts', 'inline_styles_method' );