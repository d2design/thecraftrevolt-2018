<?php
    /**
     * Registers a widget area.
     *
     * @link https://developer.wordpress.org/reference/functions/register_sidebar/
     *
     * @since Twenty Sixteen 1.0
     */
    function twentysixteen_widgets_init() {
        register_sidebar( array(
            'name' => __( 'Promo Top Left', 'twentysixteen' ),
            'id' => 'promo-left',
            'description' => __( 'Add widgets here to appear in your Hero', 'twentysixteen' ),
            'before_widget' => '<section id="%1$s" class="widget %2$s"><div class="promo-header"><div class="container "><div class="col">',
            'after_widget' => '</div></div></div></section>',
            'before_title' => '<h2 class="widget-title">',
            'after_title' => '</h2>',
        ) );

        register_sidebar( array(
            'name' => __( 'Hero', 'twentysixteen' ),
            'id' => 'hero',
            'description' => __( 'Add widgets here to appear in your Hero', 'twentysixteen' ),
            'before_widget' => '<section id="%1$s" class="widget %2$s">',
            'after_widget' => '</section>',
            'before_title' => '<h2 class="widget-title">',
            'after_title' => '</h2>',
        ) );

        register_sidebar( array(
            'name'          => __( 'Sidebar', 'twentysixteen' ),
            'id'            => 'sidebar',
            'description'   => __( 'Add widgets here to appear in your sidebar.', 'twentysixteen' ),
            'before_widget' => '<section id="%1$s" class="widget %2$s ">',
            'after_widget'  => '</section>',
            'before_title'  => '<h4 class="widget-title">',
            'after_title'   => '</h4>',
        ) );

        register_sidebar( array(
            'name'          => __( 'Footer', 'twentysixteen' ),
            'id'            => 'footer',
            'description'   => __( 'Appears at the bottom of the content on posts and pages.', 'twentysixteen' ),
            'before_widget' => '<section id="%1$s" class="widget %2$s">',
            'after_widget'  => '</section>',
            'before_title'  => '<h2 class="widget-title">',
            'after_title'   => '</h2>',
        ) );

    }
    add_action( 'widgets_init', 'twentysixteen_widgets_init' );