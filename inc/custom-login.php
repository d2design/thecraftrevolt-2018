<?php
    /**
     * Created by PhpStorm.
     * User: daviddiomede
     * Date: 6/23/18
     * Time: 2:58 PM
     */


    function custom_loginlogo()
    {
        echo '<style type="text/css">
            h1 a { 
            background-image: url(' . get_bloginfo( 'template_directory' ) . '/images/site-logo.png) !important;
            background-size:100% !important; 
            width:156px !important; 
            height:90px !important; 
            }
            body { 
            background-color: #002A3A !important; 
            }
            .login #backtoblog a, .login #nav, .login #nav a {
            color: #fff;
            }
            .login form {
            border: 2px solid #384031;
            }
            </style>';
    }

    add_action( 'login_head', 'custom_loginlogo' );

    add_filter( 'register', 'no_register_link' );
    function no_register_link( $url )
    {
        return '';
    }

    function remove_lostpassword_text( $text )
    {
        if ( $text == 'Lost your password?' ) {
            $text = '';
        }
        return $text;
    }

    add_filter( 'gettext', 'remove_lostpassword_text' );

    /* WooCommerce: The Code Below Removes Checkout Fields */
    add_filter( 'woocommerce_checkout_fields', 'custom_override_checkout_fields' );

    function custom_override_checkout_fields( $fields )
    {

        unset( $fields[ 'billing' ][ 'billing_company' ] );
//unset($fields['billing']['billing_city']);
//unset($fields['billing']['billing_postcode']);
        unset( $fields[ 'billing' ][ 'billing_country' ] );
//unset($fields['billing']['billing_state']);
        unset( $fields[ 'account' ][ 'account_username' ] );
        unset( $fields[ 'account' ][ 'account_password' ] );
        unset( $fields[ 'account' ][ 'account_password-2' ] );


        return $fields;
    }

    add_filter( "woocommerce_checkout_fields", "order_fields" );

    function order_fields( $fields )
    {

        $order = array(
            "billing_first_name",
            "billing_last_name",
            "billing_email",
            "billing_phone",
            "billing_address_1",
            "billing_address_2",
            "billing_city",
            "billing_state",
            "billing_postcode",
        );
        foreach ( $order as $field ) {
            $ordered_fields[ $field ] = $fields[ "billing" ][ $field ];
        }

        $fields[ "billing" ] = $ordered_fields;
        return $fields;

    }

    function paypal_checkout_icon()
    {
        return 'https://cdn.icomparefx.com/wp-content/uploads/2016/01/PayPal-logo-small-100x150.png';
    }

    add_filter( 'woocommerce_paypal_icon', 'paypal_checkout_icon' );

    function rs_removed_icon( $icons )
    {
        $icons = str_replace( '<img src="' . site_url() . '/wp-content/plugins/woocommerce/assets/images/icons/credit-cards/jcb.svg" alt="JCB" width="32" style="margin-left: 0.3em" />', '', $icons );
        $icons = str_replace( '<img src="' . site_url() . '/wp-content/plugins/woocommerce/assets/images/icons/credit-cards/diners.svg" alt="Diners" width="32" style="margin-left: 0.3em" />', '', $icons );
        return $icons;
    }

    add_filter( 'woocommerce_gateway_icon', 'rs_removed_icon' );


    require_once( 'wp_bootstrap_pagination.php' );

    function customize_wp_bootstrap_pagination( $args )
    {

        $args[ 'previous_string' ] = 'previous';
        $args[ 'next_string' ] = 'next';

        return $args;
    }

    add_filter( 'wp_bootstrap_pagination_defaults', 'customize_wp_bootstrap_pagination' );


    add_filter( 'the_password_form', 'custom_password_form' );
    function custom_password_form()
    {
        global $post;

        $label = 'pwbox-' . ( empty( $post->ID ) ? rand() : $post->ID );
        $o = ' 
    <head>   
    <title>The Craft Revolt</title>
    <link href="' . get_option( 'siteurl' ) . '/wp-content/themes/thecraftrevolt/css/demo.css" rel="stylesheet">
    <script src="' . get_option( 'siteurl' ) . '/wp-includes/js/jquery/jquery.js"></script>
    <script src="' . get_option( 'siteurl' ) . '/wp-content/themes/thecraftrevolt/js/jquery.validate.min.js"></script>
    <link rel="icon" href="https://thecraftrevolt.com/wp-content/uploads/2017/11/cropped-footer-logo-32x32.png" sizes="32x32" />
	<link rel="icon" href="https://thecraftrevolt.com/wp-content/uploads/2017/11/cropped-footer-logo-192x192.png" sizes="192x192" />
	<link rel="apple-touch-icon-precomposed" href="https://thecraftrevolt.com/wp-content/uploads/2017/11/cropped-footer-logo-180x180.png" />
    <style>
    	.wpcf7-form-control-wrap.email {position: relative; width: 100%; float: left;}
    	.wpcf7-form > p {display: flex;}
    	.wpcf7-form > p input.sign_up[type="submit"] {height: 40px;}
    	.wpcf7-form-control-wrap.email > label.error {position: absolute; left: 0px; width: 100%; bottom: -18px; color: #ff5a5f;}

    </style>  
    <script>
	jQuery( document ).ready(function($) {
	  $( "#open-me" ).click(function() {
	      $( ".overlay" ).addClass("overlay-open");
	      $("#password").focus();
	    });

	  $( ".close-me" ).click(function() {
	    $( ".overlay" ).removeClass( "overlay-open" );
	  });

	  
      
      $(".wpcf7-form").validate({
      	rules: {
			email: {
                required: true,
                email: true
            }
      	},
		messages: {
			email: {
                required: "Please Enter Email!",
                email: "This is not a valid email!"
            }
		}
      });


	});
	</script>  
</head>
	<body id="password-page-bg">
<div class="controls">
  <div id="open-me">
    <!--<a href="#">Enter store using password</a>-->
  </div>
</div>
    <div id="password-container" class="center">
      
      <div class="password-page-row">
          <div class="password-page-col">
            <h1 class="logo"><img src="' . get_option( 'siteurl' ) . '/wp-content/themes/thecraftrevolt/images/site-logo.png" /></h1>
            <hr class="hr-small">
          </div>
      </div>      
      <div class="password-page-row">
        <div class="password-page-col">
          <h2 class="password-page-message">Opening Soon</h2>
        </div>
      </div>      
      <div class="password-page-row">
        <div class="password-page-col">
          <h3 class="password-page-follow">Spread the word</h3>
          <div class="social-icons clearfix">  
		</div>

        </div>
      </div>
      <div class="password-page-row">
        <div class="password-page-col">
          <h3 class="password-page-follow">Find out when we open</h3>
          <div class="newsletter ">
          		' . do_shortcode( "[contact-form-7 id='502' title='Open Inquiry']" ) . '
			</div>

        </div>
      </div>
	<div class="password-page-row">
    	<div class="password-page-col">
    		<div class="password-footer">
     		  <div class="powered">
  	  		  &copy; 2017 Craft Revolt.<br>
      		  </div>
     		</div>
   		 </div>
  	</div>
  </div>

<div class="overlay overlay-data">
<span class="close-me controls"><div class="pass-close"><a href="#"><span class="icon-close"></span></a></div></span>
<div class="modalbox">
  <div class="inputbox">
    <form method="post" action="' . get_option( 'siteurl' ) . '/craftvoltadmin/?action=postpass" id="login_form" class="storefront-password-form">
    <input type="hidden" value="storefront_password" name="form_type" />
      <label for="password">Password </label>
      <input name="post_password" placeholder="Your password" id="' . $label . '" type="password" class="form-control" size="20" />
      <div class="actions">
        <input type="submit" name="Submit" class="action_button sign_up" value="' . esc_attr__( "Enter" ) . '" />
      </div>
    </form>

    <div id="owner">Are you the store owner? <a href="#" class="text-link">Log in here</a> or <a href="#">change your password settings</a></div>
  </div>
</div>
</div>
</body>
    ';
        return $o;
    }

