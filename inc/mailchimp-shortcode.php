<?php
    /**
     * Mailchimp Shortcode.
     *
     */


    function mailchimp_shortcode( $atts )
    {

        shortcode_atts( [
            'title' => esc_html( '' )
        ], $atts );

        $html = '<div id="mc_embed_signup">';
        $html .= '<form 
                    action="https://thecraftrevolt.us17.list-manage.com/subscribe/post?u=4e3a46583f7a4c18e57c40d05&id=633de34566" 
                    method="post" id="mc-embedded-subscribe-form" 
                    name="mc-embedded-subscribe-form" 
                    class="validate" 
                    target="_blank" 
                    novalidate>';
        $html .= '<div id="mc_embed_signup_scroll">';
        $html .= '<div class="form-row">';
        $html .= '<div class="col-lg-3 col-md-12 mt-2">';
        $html .= '<h5 class="align-middle">';
        $html .= $atts[ 'title' ];
        $html .= '</h5>';
        $html .= '</div>';
        $html .= '<div class="col col-lg-3 col-sm-4">';
        $html .= '<input type="email" value="" name="EMAIL" class="form-control" placeholder="Email" id="mce-EMAIL">';
        $html .= '</div>';
        $html .= '<div class="col  col-lg-3 col-sm-4">';
        $html .= '<input type="text" value="" name="MMERGE5" class="form-control" placeholder="ZIP Code" id="mce-MMERGE5">';
        $html .= '</div>';
        $html .= '<div class="col  col-lg-3 col-sm-4">';
        $html .= '<input type="submit" value="Subscribe" name="subscribe" id="mc-embedded-subscribe" class="btn btn-primary">';
        $html .= '</div>';
        $html .= '</div>';
        $html .= '<div id="mce-responses" class="clear">';
        $html .= '<div class="response" id="mce-error-response" style="display:none"></div>';
        $html .= '<div class="response" id="mce-success-response" style="display:none"></div>';
        $html .= '</div>';
        $html .= '<div style="position: absolute; left: -5000px;" aria-hidden="true">';
        $html .= '<input type="text" name="b_4e3a46583f7a4c18e57c40d05_633de34566" tabindex="-1" value=""></div>';
        $html .= '</div>';
        $html .= '</form>';
        $html .= '</div>';


        wp_enqueue_script( 'mc-validate', 'https://s3.amazonaws.com/downloads.mailchimp.com/js/mc-validate.js', array(), '1.0.0', true );


        return $html;
    }

    add_shortcode( 'subscribe', 'mailchimp_shortcode' );