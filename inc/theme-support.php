<?php
    /**
     * Twenty Sixteen Setup and theme support
     *
     */


    if ( !function_exists( 'twentysixteen_setup' ) ) :
        /**
         * Sets up theme defaults and registers support for various WordPress features.
         *
         * Note that this function is hooked into the after_setup_theme hook, which
         * runs before the init hook. The init hook is too late for some features, such
         * as indicating support for post thumbnails.
         *
         * Create your own twentysixteen_setup() function to override in a child theme.
         *
         * @since Twenty Sixteen 1.0
         */
        function twentysixteen_setup()
        {
            /*
             * Make theme available for translation.
             * Translations can be filed at WordPress.org. See: https://translate.wordpress.org/projects/wp-themes/twentysixteen
             * If you're building a theme based on Twenty Sixteen, use a find and replace
             * to change 'twentysixteen' to the name of your theme in all the template files
             */
            load_theme_textdomain( 'twentysixteen' );

            // Add default posts and comments RSS feed links to head.
            add_theme_support( 'automatic-feed-links' );

            /*
             * Let WordPress manage the document title.
             * By adding theme support, we declare that this theme does not use a
             * hard-coded <title> tag in the document head, and expect WordPress to
             * provide it for us.
             */
            add_theme_support( 'title-tag' );

            /*
             * Enable support for custom logo.
             *
             *  @since Twenty Sixteen 1.2
             */
            add_theme_support( 'custom-logo', array(
                'height' => 'auto',
                'width' => 180,
                'flex-height' => true,
                'flex-width' => true,
            ) );

            /*
             * Enable support for Post Thumbnails on posts and pages.
             *
             * @link http://codex.wordpress.org/Function_Reference/add_theme_support#Post_Thumbnails
             */
            add_theme_support( 'post-thumbnails' );
            set_post_thumbnail_size( 1200, 9999 );

            // This theme uses wp_nav_menu() in two locations.
            register_nav_menus( array(
                'primary' => __( 'Primary Menu Left', 'twentysixteen' ),
                'primary2' => __( 'Primary Menu Right', 'twentysixteen' ),
                'secondary' => __( 'Secondary Menu Left', 'twentysixteen' ),
                'secondary2' => __( 'Secondary Menu Right', 'twentysixteen' ),
                'footer' => __( 'Footer Menu', 'twentysixteen' ),
                'social' => __( 'Social Links Menu', 'twentysixteen' ),
                'class_views' => __( 'Class Views Menu', 'twentysixteen' ),
            ) );

            /*
             * Switch default core markup for search form, comment form, and comments
             * to output valid HTML5.
             */
            add_theme_support( 'html5', array(
                'search-form',
                'comment-form',
                'comment-list',
                'gallery',
                'caption',
            ) );

            /*
             * Enable support for Post Formats.
             *
             * See: https://codex.wordpress.org/Post_Formats
             */
            add_theme_support( 'post-formats', array(
                'aside',
                'image',
                'video',
                'quote',
                'link',
                'gallery',
                'status',
                'audio',
                'chat',
            ) );


            // Indicate widget sidebars can use selective refresh in the Customizer.
            add_theme_support( 'customize-selective-refresh-widgets' );
        }
    endif; // twentysixteen_setup

    add_action( 'after_setup_theme', 'twentysixteen_setup' );

    /**
     * Sets the content width in pixels, based on the theme's design and stylesheet.
     *
     * Priority 0 to make it available to lower priority callbacks.
     *
     * @global int $content_width
     *
     * @since Twenty Sixteen 1.0
     */
    function twentysixteen_content_width()
    {
        $GLOBALS[ 'content_width' ] = apply_filters( 'twentysixteen_content_width', 840 );
    }

    add_action( 'after_setup_theme', 'twentysixteen_content_width', 0 );

    // Add support for excerpt on pages
    add_post_type_support( 'page', 'excerpt' );

    //Disable emoji support
    function disable_wp_emojicons() {

        // all actions related to emojis
        remove_action( 'admin_print_styles', 'print_emoji_styles' );
        remove_action( 'wp_head', 'print_emoji_detection_script', 7 );
        remove_action( 'admin_print_scripts', 'print_emoji_detection_script' );
        remove_action( 'wp_print_styles', 'print_emoji_styles' );
        remove_filter( 'wp_mail', 'wp_staticize_emoji_for_email' );
        remove_filter( 'the_content_feed', 'wp_staticize_emoji' );
        remove_filter( 'comment_text_rss', 'wp_staticize_emoji' );

        // filter to remove TinyMCE emojis
        add_filter( 'tiny_mce_plugins', 'disable_emojicons_tinymce' );
    }
    add_action( 'init', 'disable_wp_emojicons' );

    function disable_emojicons_tinymce( $plugins ) {
        if ( is_array( $plugins ) ) {
            return array_diff( $plugins, array( 'wpemoji' ) );
        } else {
            return array();
        }
    }

    add_filter( 'emoji_svg_url', '__return_false' );
