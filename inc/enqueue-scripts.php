<?php
    /**
     * Enqueues scripts and styles.
     *
     * @since Twenty Sixteen 1.0
     */


    function _remove_script_version( $src ){
        $parts = explode( '?', $src );
        return $parts[0];
    }
    add_filter( 'script_loader_src', '_remove_script_version', 15, 1 );
    //add_filter( 'style_loader_src', '_remove_script_version', 15, 1 );

    /**
     * Serve CSS and JS from hashed filenames.
     *
     * Checks for hashed filename as a value in JSON object.
     * If it exists the hashed file is enqueued.
     *
     * @param string
     * @return string $asset
     */
    function get_asset_path( $asset )
    {

        $map = get_template_directory() . '/dist/data/mainifest.json';
        static $hash = null;

        if ( null === $hash ) {
            $hash = file_exists( $map ) ? json_decode( file_get_contents( $map ), true ) : [];
        }

        if ( array_key_exists( $asset, $hash ) ) {
            return '/dist/' . $hash[ $asset ];
        }

        return $asset;
    }

    /**
     * Handles JavaScript detection.
     *
     * Adds a `js` class to the root `<html>` element when JavaScript is detected.
     *
     * @since Twenty Sixteen 1.0
     */
    function twentysixteen_javascript_detection()
    {
        echo "<script>(function(html){html.className = html.className.replace(/\bno-js\b/,'js')})(document.documentElement);</script>\n";
    }

    add_action( 'wp_head', 'twentysixteen_javascript_detection', 0 );

    /**
     * Enqueues scripts and styles.
     *
     * @since Twenty Sixteen 1.0
     */

    function twentysixteen_scripts()
    {
        // Deregister the jquery version bundled with WordPress.
        //wp_deregister_script( 'jquery' );

        // CDN hosted jQuery placed in the header, as some plugins require that jQuery is loaded in the header.
        wp_enqueue_script( 'jquery', 'https://ajax.googleapis.com/ajax/libs/jquery/3.3.1/jquery.min.js', array(), '3.3.1', true );


        wp_enqueue_script( 'twentysixteen-skip-link-focus-fix', get_template_directory_uri() . '/js/skip-link-focus-fix.js', array(), '20160816', true );

        if ( is_singular() && comments_open() && get_option( 'thread_comments' ) ) {
            wp_enqueue_script( 'comment-reply' );
        }

        if ( is_singular() && wp_attachment_is_image() ) {
            wp_enqueue_script( 'twentysixteen-keyboard-image-navigation', get_template_directory_uri() . '/js/keyboard-image-navigation.js', array( 'jquery' ), '20160816' );
        }

        wp_enqueue_script( 'twentysixteen-script', get_template_directory_uri() . '/js/functions.js', array( 'jquery' ), '20160816', true );

        wp_localize_script( 'twentysixteen-script', 'screenReaderText', array(
            'expand' => __( 'expand child menu', 'twentysixteen' ),
            'collapse' => __( 'collapse child menu', 'twentysixteen' ),
        ) );
    }

    // Add custom fonts, used in the main stylesheet.
    wp_enqueue_style( 'custom-google-fonts', 'https://fonts.googleapis.com/css?family=Montserrat:400,500,700', array(), '4.9', 'all' );

    // Add Genericons, used in the main stylesheet.
    wp_enqueue_style( 'genericons', get_template_directory_uri() . '/genericons/genericons.css', array(), '3.4.1' );

    // Add Font Awesome
    wp_enqueue_style('font-awesome', 'https://use.fontawesome.com/releases/v5.2.0/css/all.css', array(), '5.2.0'  );

    // Enqueue Styles
    wp_enqueue_style( 'theme-styles', get_stylesheet_directory_uri() . get_asset_path( 'main.css' ) );

    /// Enqueue Scripts
    wp_enqueue_script( 'main-scripts', get_template_directory_uri() . get_asset_path( 'main.js' ) );

    wp_enqueue_script( 'twentysixteen-moment', get_template_directory_uri() . '/js/moment.js', array(), filemtime( get_template_directory() . '/js/moment.js' ) );
    wp_enqueue_script( 'twentysixteen-calender', get_template_directory_uri() . '/js/fullcalendar.min.js', array(), '20160816', true );
    wp_enqueue_script( 'twentysixteen-classie', get_template_directory_uri() . '/js/classie.js', array(), '20160816', true );
    wp_enqueue_script( 'popper', 'https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.14.3/umd/popper.min.js', array(), '1.14.3', true );
    wp_enqueue_script( 'twentysixteen-bootstrap-js', get_template_directory_uri() . '/js/bootstrap.min.js', array(), '20160816', true );


    add_action( 'wp_enqueue_scripts', 'twentysixteen_scripts' );

    /**
     * Registers an editor stylesheet for the theme.
     */
    function wpdocs_theme_add_editor_styles()
    {
        add_editor_style( get_stylesheet_directory_uri() . get_asset_path( 'main.css' ) );
    }

    add_action( 'admin_init', 'wpdocs_theme_add_editor_styles' );

