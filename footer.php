<?php
    /**
     * The template for displaying the footer
     *
     * Contains the closing of the #content div and all content after
     *
     * @package WordPress
     * @subpackage Twenty_Sixteen
     * @since Twenty Sixteen 1.0
     */
?>

<!----------- Footer ------------>
<footer class="footer-bs bg-dark">
    <div class="row">
        <div class="col-md-3 footer-brand">
            <div class="navbar-brand"><?php the_custom_logo(); ?></div>
            <?php dynamic_sidebar( 'footer' ); ?>
            <p>&copy; <?php echo date( "Y" ); ?>
                <?php bloginfo( 'name' ) ?>, All rights reserved</p>
        </div>
        <div class="col-md-3 footer-nav">
            <h4>Menu —</h4>

            <?php
                wp_nav_menu( array(
                    'theme_location' => 'footer',
                    'depth' => 2,
                    'container' => 'div',
                    'container_class' => '',
                    'container_id' => '',
                    'menu_class' => 'nav navbar-nav',
                    'fallback_cb' => 'WP_Bootstrap_Navwalker::fallback',
                    'walker' => new WP_Bootstrap_Navwalker(),
                ) );
            ?>

        </div>
        <div class="col-md-3 footer-social">
            <h4>Follow Us</h4>
            <?php
                wp_nav_menu( array(
                    'theme_location' => 'social',
                    'depth' => 1,
                    'container' => 'div',
                    'container_class' => '',
                    'container_id' => '',
                    'menu_class' => '',
                    'fallback_cb' => 'WP_Bootstrap_Navwalker::fallback',
                    'walker' => new WP_Bootstrap_Navwalker(),
                ) );
            ?>
        </div>
        <?php require_once( 'template-parts/footer-signup.php' ); ?>
</footer>

<?php wp_footer(); ?>
<script>
    window.onscroll = function () {
        myFunction()
    };

    const navbar = document.getElementById('navbar');
    const sticky = navbar.offsetTop;

    function myFunction() {
        if (window.pageYOffset >= sticky) {
            navbar.classList.add('fixed-top')
        } else {
            navbar.classList.remove('fixed-top');
        }
    }
</script>
</body>
</html>
