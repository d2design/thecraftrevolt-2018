<?php
    /**
     * Template Name: Right Sidebar Page
     *
     * This is the template that displays all pages by default.
     * Please note that this is the WordPress construct of pages and that
     * other "pages" on your WordPress site will use a different template.
     *
     * @package WordPress
     * @subpackage Twenty_Sixteen
     * @since Twenty Sixteen 1.0
     */

    get_header();
?>
<article id="post-<?php the_ID(); ?>" <?php post_class(); ?>>
    <div class="container mt-5" id="main">
        <div class="row">

            <?php if ( have_posts() ) : while ( have_posts() ) : the_post(); ?>

                <article class="page col-md-8" id="post-<?php the_ID(); ?>">
                    <div class="entry">
                            <?php get_template_part( 'template-parts/content' ); ?>

                    </div>
                </article>
            <?php endwhile; else: ?>
                <article class="page col-md-8 not-found">
                    <div class="entry">
                        <p class="lead"><?php _e('Sorry, this page does not exist. Try searching for one.'); ?></p>
                        <?php get_search_form(); ?>
                    </div>
                </article>
            <?php endif; ?>

            <?php get_sidebar(); ?>

        </div> <!-- .row -->
    </div> <!-- .container -->
<?php get_footer(); ?>