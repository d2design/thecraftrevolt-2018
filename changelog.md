# Changelog

## 2.5.2
* Use smaller image sizes on event cards
    
## 2.5.1
* Add eventon taxonomy page
    * Removed extra description to help improve SEO

## 2.5.0
* Right sidebar page updates
* Update page header to add featured image
* Use new image sizes to set featured image as css
* Fix broken path to google fonts

## 2.4.0
* Fix broken closing tag on checkout page

## 2.3.5
* Fix broken closing tag on checkout page

## 2.3.4
* Remove sidebar from bottom of page

## 2.3.3
* Remove queries from scripts

## 2.3.2
* Remove Header form as hard coded
Remove mailchimp validate
* Causes conflicts and blocks admin usage

## 2.3.1
* Temporary Mailchimp Promo fix

## 2.3
* Fix spacing on my account forms
* Add editor styles and woocommerce overrides
* Sidebar widget area for sidebar page
* Remove extra unused widget areas
* Remove unused comments area
* Add right sidebar page
* Remove sidebar reference
* Cleanup unused widget areas
* Bugfix: Mailchip signup shortcode
* Add lucky orange tracking to js
* Remove tracking file reference
* Add font awesome from CDN

## 2.2.2
* Display forgot password text

## 2.2.1
* Wordpress gallery styles

## 2.2
* Fix account page column widths

## 2.1
* Display only top level event type categories

## 2.0
* Style updates and consolidation
* Custom shortcode for registration
* New header that allows for widgets
* Woocommerce cart menu
* Add support for featured pages
* Support for page excerpts

## 1.3
Add Webpack build to process Javascript SCSS 
* Instantiate styles from manifest
* Creates hashed files with manifest log
* CSS
* JS
* Enqueue styles from manifest

## 1.3.1
Add tracking scripts

## 1.3.2
Remove Google Analytics tracking scripts