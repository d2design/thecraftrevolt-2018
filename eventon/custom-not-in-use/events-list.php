<?php
/* Template Name: Events List */

get_header();
?>
<section class="grid-view-page">
    <div class="container">
      <?php get_template_part( 'template-parts/home/search', '' ); ?>
      <div class="row">
        <?php
        if ( get_query_var('paged') ) {
            $paged = get_query_var('paged');
        } elseif ( get_query_var('page') ) { // 'page' is used instead of 'paged' on Static Front Page
            $paged = get_query_var('page');
        } else {
            $paged = 1;
        }
        if(isset($_GET)) {
          $custom_query_args = array(
            'post_type' => 'ajde_events', 
            'posts_per_page' => get_option('posts_per_page'),
            'paged' => $paged,
            'post_status' => 'publish',
            'ignore_sticky_posts' => true,
            'order' => 'DESC',
            's' => $_GET['keyword'],
            'cat_id' => $_GET['cat_id'],
            'orderby' => 'date',
            'meta_query' => array(
                array(
                    'key'     => 'evcal_srow',
                    'value'   => strtotime($_GET['date']),
                    'compare' => '>=',
                ),
            ),
          );
        } else {
          $custom_query_args = array(
            'post_type' => 'ajde_events', 
            'posts_per_page' => get_option('posts_per_page'),
            'paged' => $paged,
            'post_status' => 'publish',
            'ignore_sticky_posts' => true,
            'order' => 'DESC',
            'orderby' => 'date'
          );
        }
        
        $custom_query = new WP_Query( $custom_query_args );

        if ( $custom_query->have_posts() ) :
            while( $custom_query->have_posts() ) : $custom_query->the_post();
          $img_id =get_post_thumbnail_id(get_the_ID());
          $img_src = wp_get_attachment_image_src($img_id,'thumbnail');
          $ProId = get_post_meta(get_the_ID(), 'tx_woocommerce_product_id' );
          $Orgterms = wp_get_object_terms( get_the_ID(), 'event_organizer' );
          $pmv = get_post_meta(get_the_ID());
          //print_r($pmv);
          $start = (!empty($pmv['evcal_srow'])?$pmv['evcal_srow'][0]:'');
          if(!empty($start)){
              // date and time as separate columns
              $startdate= date( apply_filters('evo_csv_export_dateformat','m/d/Y'), $start);
              $starttime= date( apply_filters('evo_csv_export_timeformat','h:i:A'), $start);
              //$csvRow.= '"'. date($date_time_format, $start) .'",';
            }
          $end = (!empty($pmv['evcal_erow'])?$pmv['evcal_erow'][0]:'');
          if(!empty($end)){
              // date and time as separate columns
              $enddate= date( apply_filters('evo_csv_export_dateformat','m/d/Y'), $end);
              $endtime= date( apply_filters('evo_csv_export_timeformat','h:i:A'), $end);
              //$csvRow.= '"'. date($date_time_format,$end) .'",';
            }
          $Locterms = wp_get_object_terms( get_the_ID(), 'event_location' );
        ?>
        <div class="col-12">
          <div class="list-box mb-4 pb-3">
            <div class="d-flex">
              <div class="image text-center mb-3">
                <img src="<?php echo $img_src[0];?>" alt="" class="img-fluid">
              </div>
              <div class="caption px-3 py-0">
                <div class="w-100">
                  <h5 class="font-weight-bold text-truncate pb-0"><a href="<?php the_permalink(); ?>"><?php the_title(); ?></a></h5>
                  <div class="text">
                    <?php the_excerpt();?>
                  </div>
                </div>
              </div>
            </div>
            <div class="d-flex pb-3">
              <div class="pr-lg-5 pr-2"><span class="price">$<?php echo $pmv['evotx_price'][0];?> USD</span></div>
              <?php
                if(!empty($Orgterms[0])) {
                ?>
              <div class="pr-lg-5 pr-2"><b>Teacher:</b> <?php echo $Orgterms[0]->name;?></div>
              <?php } ?>
              <div class=""><b>Location:</b> <?php echo $Locterms[0]->name;?></div>
            </div>

            <div class="d-flex pb-1">
              <div class="pr-lg-5 pr-2">
                <div class="d-flex">
                  <div class="pr-lg-4 pr-1"><i class="fa fa-calendar"></i>  <?php echo $startdate;?></div>
                  <div class=""><i class="fa fa-clock-o"></i>  <?php echo $starttime;?> - <?php echo $endtime;?></div>
                </div>
              </div>
              <?php
              $evcal_repeat = (!empty($pmv["evcal_repeat"]) )? $pmv["evcal_repeat"][0]:null;
                        $count =0;
                        if(!empty($pmv['repeat_intervals'])){
              ?>
              <div class="pr-lg-5 pr-2">
                <div class="d-flex">
                  <div class="" style="position: relative;">
                    <!-- hover div -->
                    <a class="grid-view-link">View All</a>
                    <div class="grid-view-all-dates">
                      <table class="table table-bordered">
                        <?php
                        
                                $repeat_times = (unserialize($pmv['repeat_intervals'][0]));
                                //print_r($repeat_times);

                                // datre format sting to display for repeats
                                $date_format_string = $evcal_date_format[1].' '.( $evcal_date_format[2]? 'G:i':'h:ia');
                                
                                foreach($repeat_times as $rt){
                                  $startUNIX = (int)$rt[0];
                                  $endUNIX = (int)$rt[1];
                                  /*echo '<li data-cnt="'.$count.'" style="display:'.(( $count>3)?'none':'block').'" class="'.($count==0?'initial':'').($count>3?' over':'').'">'. ($count==0? '<dd>'.__('Initial','eventon').'</dd>':'').'<span>'.__('from','eventon').'</span> '.date($date_format_string,$startUNIX).' <span class="e">End</span> '.date($date_format_string,$endUNIX).'
                                  </li>';*/
                                  
                                
                              /*echo ( !empty($pmv['repeat_intervals']))? 
                                "<p class='evo_custom_repeat_list_count' data-cnt='{$count}' style='padding-bottom:20px'>There are ".($count-1)." repeat intervals. ". ($count>3? "<span class='evo_repeat_interval_view_all' data-show='no'>".__('View All','eventon')."</span>":'') ."</p>"
                                :null;*/
                        ?>
                        <tr>
                          <td><i class="fa fa-calendar"></i>  <?php echo date('m-d-Y',$startUNIX);?></td> 
                          <td><i class="fa fa-clock-o"></i>  <?php echo date($date_format_string,$startUNIX);?> - <?php echo date($date_format_string,$endUNIX);?></td>
                        </tr>
                        <?php 
                        $count++;
                          }
                        ?>
                      </table>
                    </div>
                    <!-- end hover div -->
                    

                  </div>
                </div>
              </div>
              <?php } ?>
            </div>

          </div>
        </div>
        <?php
          endwhile;
        ?>

        <div class="col-12 d-flex justify-content-center pb-4">
          <?php if ($custom_query->max_num_pages > 1) :  ?>
            <?php
            $orig_query = $wp_query;
            $wp_query = $custom_query;
            ?>
            <nav class="prev-next-posts">
                <div class="prev-posts-link">
                    <?php echo get_next_posts_link( 'Older Entries', $custom_query->max_num_pages ); ?>
                </div>
                <div class="next-posts-link">
                    <?php echo get_previous_posts_link( 'Newer Entries' ); ?>
                </div>
            </nav>
            <?php
            $wp_query = $orig_query;
            ?>
        <?php endif; ?>
          <!--<ul class="pagination">
            <li class="page-item"><a class="page-link" href="#"><i class="fa fa-angle-left"></i></a></li>
            <li class="page-item"><a class="page-link" href="#">1</a></li>
            <li class="page-item"><a class="page-link" href="#">2</a></li>
            <li class="page-item"><a class="page-link" href="#">3</a></li>
            <li class="page-item"><a class="page-link" href="#"><i class="fa fa-angle-right"></i></a></li>
          </ul>-->
        </div>
          <?php
              wp_reset_postdata();
          else:
              echo '<div class="col-12 d-flex justify-content-center">'.__('Sorry, no posts matched your criteria.').'</div>';
          endif;
          ?>
      </div>
    </div>
  </section>
<?php get_footer();
