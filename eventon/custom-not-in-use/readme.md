### Templates that are not in use ###

These templates were removed from the original version of this theme to allow full use of the EventOn plugin's default setup, 
by Shannon MacMillan, Blue Storm Creative, 2/2018 as per Libby Hays instruction.
shannon@bluestormcreative.com
