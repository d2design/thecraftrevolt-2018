<?php
    /**
     * Twenty Sixteen functions and definitions
     *
     * Set up the theme and provides some helper functions, which are used in the
     * theme as custom template tags. Others are attached to action and filter
     * hooks in WordPress to change core functionality.
     *
     * When using a child theme you can override certain functions (those wrapped
     * in a function_exists() call) by defining them first in your child theme's
     * functions.php file. The child theme's functions.php file is included before
     * the parent theme's file, so the child theme functions would be used.
     *
     * @link https://codex.wordpress.org/Theme_Development
     * @link https://codex.wordpress.org/Child_Themes
     *
     * Functions that are not pluggable (not wrapped in function_exists()) are
     * instead attached to a filter or action hook.
     *
     * For more information on hooks, actions, and filters,
     * {@link https://codex.wordpress.org/Plugin_API}
     *
     * @package WordPress
     * @subpackage Twenty_Sixteen
     * @since Twenty Sixteen 1.0
     */

    /** Rewrite Rules Support */
    require_once( 'inc/rewrite-rules.php' );

    /** Enqueue scripts */
    //require_once( 'inc/enqueue-styles.php' );

    /** Body class support */
    require_once( 'inc/body-class-support.php' );

    /** Calendar Styles */
    require_once( 'inc/calendar-view.php' );

    /** Create widget areas in sidebar and footer */
    require_once( 'inc/widget-areas.php' );

    /** Customizer support */
    require_once( 'inc/custom-login.php' );

    /** Add theme support */
    require_once( 'inc/theme-support.php' );

    /** Custom Login Screen */
    require_once( 'inc/woo-commerce-nav.php' );

    /** Image support */
    require_once( 'inc/image-support.php' );

    // Register Custom Navigation Walker
    require_once( 'inc/class-wp-bootstrap-navwalker.php' );

    /** Enqueue scripts */
    require_once( 'inc/enqueue-scripts.php' );

    /** Custom template tags for this theme. */
    require_once( 'inc/template-tags.php' );

    /** Custom template tags for this theme. */
    require_once( 'inc/page_categories.php' );

    /** Customizer additions. */
    require_once( 'inc/customizer.php' );

    /** Widget areas */
    require_once( 'inc/hide-updates.php' );

    /** Custom Login Screen */
    require_once( 'inc/custom-login.php' );

    /** MailChimp Shortcode Register */
    require_once( 'inc/mailchimp-shortcode.php' );

    /** Inline Featured Image */
    require_once( 'inc/inline-featured-image.php' );


