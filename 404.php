<?php
/**
 * The template for displaying 404 pages (not found)
 *
 * @package WordPress
 * @subpackage Twenty_Sixteen
 * @since Twenty Sixteen 1.0
 */

get_header(); ?>

	<div id="primary" class="content-area">
		<main id="main" class="container" role="main">

			<section class="error-404 not-found d-flex flex-wrap py-5">
				<div class="row w-100">
					<div class="col-lg-8 mx-auto col-12">
						<header class="page-header w-100">
							<h1 class="page-title"><?php _e( 'Oops! That page can&rsquo;t be found.', 'twentysixteen' ); ?></h1>
						</header><!-- .page-header -->

						<div class="page-content w-100">
							<p><?php _e( 'It looks like nothing was found at this location. Maybe try a search?', 'twentysixteen' ); ?></p>

							<?php //get_search_form(); ?>
							<form class="form-inline mt-4 mr-3" role="search" method="get" action="<?php echo home_url( '/' ); ?>">
							  <label class="w-100 justify-content-start pb-2 font-weight-bold" for="">Search for</label>
					          <div class="input-group w-100">
					            <input type="text" class="form-control" placeholder="What are you Looking for?" name="s" aria-label="Search for...">
					            <span class="input-group-btn">
					              <button class="btn btn-primary" type="submit"><i class="fa fa-search"></i></button>
					            </span>
					          </div>
					        </form>
						</div><!-- .page-content -->
					</div>
				</div>
			</section><!-- .error-404 -->

		</main><!-- .site-main -->

		<?php get_sidebar( 'content-bottom' ); ?>

	</div><!-- .content-area -->

<?php get_sidebar(); ?>
<?php get_footer(); ?>
