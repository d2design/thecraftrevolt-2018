<?php
/**
 WP Post Template: Teacher Profile
 */

get_header();
 global $post;
?>
<div id="primary" class="content-area">
  <main id="main" class="site-main" role="main">
    <?php
    // Start the loop.
    while ( have_posts() ) : the_post();
    $pmv = get_post_meta(get_the_ID());
    $slug = get_post_field( 'post_name', get_post() );
    $about = get_post_meta(get_the_ID(),'wpcf-about',true);
    $workshops = get_post_meta(get_the_ID(),'wpcf-workshops',true);
      ?>


    <!-- Teacher Profile Start -->
    <section class="teacher-profile">
        <div class="container">
          <div class="profile-section">
            <div class="row mb-4">
              <div class="col-md-2 image mb-3">
                <?php //twentysixteen_post_thumbnail('thumbnail'); 
                      the_post_thumbnail( 'thumbnail' );
                ?>
              </div>
              <div class="col-md-10 col-xs-12">
                <h3>
                    <?php if ( is_sticky() && is_home() && ! is_paged() ) : ?>
                      <span class="sticky-post"><?php _e( 'Featured', 'twentysixteen' ); ?></span>
                    <?php endif; ?>

                    <?php the_title( sprintf( '<a href="%s" rel="bookmark">', esc_url( get_permalink() ) ), '</a>' ); ?>
                </h3>
                <!--<p><label>Subject:</label> Craft, Art, Craft</p>-->
                <div>
                    <?php
                    echo "<b>About: </b><br />" . $about; ?>
                </div>
              </div>
              <div class="col-12">
                  <?php echo "<b>Workshops: </b><br />" . $workshops; ?>
              </div>
            </div>
          </div>
          <?php //echo do_shortcode("[add_eventon event_organizer='22']");?>
          <div class="row">
            <div class="col-md-12">
              <h3 class="text-center mb-4">Sign up</h3>
              <table class="table table-bordered text-center teacher-view">
                <thead>
                  <tr>
                    <th>Class Title</th>
                    <th>Date</th>
                    <th>Time</th>
                    <th>Action</th>
                  </tr>
                </thead>
                <tbody>
                  <?php
                  $term_slug = $slug;
                  $taxonomies = get_taxonomies();
                  foreach ( $taxonomies as $tax_type_key => $taxonomy ) {
                      // If term object is returned, break out of loop. (Returns false if there's no object)
                      if ( $term_object = get_term_by( 'slug', $term_slug , $taxonomy ) ) {
                          break;
                      }
                  }
                  $term_id = $term_object->term_id;

          //var_dump( $term_object );

                  $custom_query_args = array(
                    'post_type' => 'ajde_events',
                    'post_status' => 'publish',
                    'ignore_sticky_posts' => true,
                    'order' => 'ASC',
                    'orderby' => 'date',
                    'tax_query' => array(
                        array(
                            'taxonomy' => 'event_organizer',
                            'field'   => 'id',
                            'terms' => $term_id,
                        ),
                    ),
                  );
                  $custom_query = new WP_Query( $custom_query_args );

                  if ( $custom_query->have_posts() ) :
                      while( $custom_query->have_posts() ) : $custom_query->the_post();
                    $ProId = get_post_meta(get_the_ID(), 'tx_woocommerce_product_id' );
                    $Orgterms = wp_get_object_terms( get_the_ID(), 'event_organizer' );
                    $pmv = get_post_meta(get_the_ID());
                    // /print_r($pmv);
                    $start = (!empty($pmv['evcal_srow'])?$pmv['evcal_srow'][0]:'');
                    if(!empty($start)){
                        // date and time as separate columns
                        $startdate= date( apply_filters('evo_csv_export_dateformat','m/d/Y'), $start);
                        $starttime= date( apply_filters('evo_csv_export_timeformat','h:i:A'), $start);
                        //$csvRow.= '"'. date($date_time_format, $start) .'",';
                      }
                    $end = (!empty($pmv['evcal_erow'])?$pmv['evcal_erow'][0]:'');
                    if(!empty($end)){
                        // date and time as separate columns
                        $enddate= date( apply_filters('evo_csv_export_dateformat','m/d/Y'), $end);
                        $endtime= date( apply_filters('evo_csv_export_timeformat','h:i:A'), $end);
                        //$csvRow.= '"'. date($date_time_format,$end) .'",';
                      }
                  ?>
                  <tr>
                    <td><?php the_title(); ?></td>
                    <td><?php echo $startdate;?></td>
                    <td><?php echo $starttime;?> - <?php echo $endtime;?></td>
                    <td><a href="<?php the_permalink(); ?>" class="btn btn-view">View</a></td>
                  </tr>
                  <?php
                    endwhile;
                  ?>

                  <?php
                        wp_reset_postdata();
                    else:
                        echo '<tr><td colspan="4">'.__('Sorry, no class found.').'</td></tr>';
                    endif;
                    ?>
                </tbody>
              </table>
            </div>
            <div class="col-12">
                <?php 
                    /* translators: %s: Name of current post */
                    the_content( sprintf(
                        __( 'Continue reading<span class="screen-reader-text"> "%s"</span>', 'twentysixteen' ),
                        get_the_title()
                    ) );
                ?>
            </div>
          </div>
        </div>
    </section>
    <!-- Teacher Profile End -->


<?php

      // End of the loop.
    endwhile;
    ?>

  </main><!-- .site-main -->

  <?php get_sidebar( 'content-bottom' ); ?>

</div><!-- .content-area -->
<?php get_sidebar(); ?>
<?php get_footer();
