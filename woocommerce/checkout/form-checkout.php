<?php
    /**
     * Checkout Form
     *
     * This template can be overridden by copying it to yourtheme/woocommerce/checkout/form-checkout.php.
     *
     * HOWEVER, on occasion WooCommerce will need to update template files and you
     * (the theme developer) will need to copy the new files to your theme to
     * maintain compatibility. We try to do this as little as possible, but it does
     * happen. When this occurs the version of the template file will be bumped and
     * the readme will list any important changes.
     *
     * @see        https://docs.woocommerce.com/document/template-structure/
     * @author        WooThemes
     * @package    WooCommerce/Templates
     * @version     2.3.0
     */

    if ( !defined( 'ABSPATH' ) ) {
        exit;
    }

    wc_print_notices();

    do_action( 'woocommerce_before_checkout_form', $checkout );

    // If checkout registration is disabled and not logged in, the user cannot checkout
    if ( !$checkout->is_registration_enabled() && $checkout->is_registration_required() && !is_user_logged_in() ) {
        echo apply_filters( 'woocommerce_checkout_must_be_logged_in_message', __( 'You must be logged in to checkout.', 'woocommerce' ) );
        return;
    }

?>
<div class="checkout-form w-100">

    <form name="checkout" method="post" class="checkout woocommerce-checkout"
          action="<?php echo esc_url( wc_get_checkout_url() ); ?>" enctype="multipart/form-data">
        <div class="col-lg-10 col-12 mx-auto">
            <div class="row">
                <div class="col-md-12 col-12">
                    <div class="form-group">
                        <a class="button alt" href="<?php echo site_url() ?>/cart">View Cart</a>
                    </div>
                </div>
            </div>

            <?php if ( $checkout->get_checkout_fields() ) : ?>

                <?php do_action( 'woocommerce_checkout_before_customer_details' ); ?>

                <div class="col2-set row d-none1" id="customer_details">
                    <div class="col-12">
                        <?php do_action( 'woocommerce_checkout_billing' ); ?>
                    </div>

                    <div class="col-12">
                        <?php do_action( 'woocommerce_checkout_shipping' ); ?>
                    </div>
                </div>

                <?php do_action( 'woocommerce_checkout_after_customer_details' ); ?>

            <?php endif; ?>

            <div class="row">
                <div class="col-md-12 col-12 text-center py-3">
                    <h3 class="text-center" id="order_review_heading"><?php _e( 'Your order', 'woocommerce' ); ?></h3>
                </div>
                <div class="col-12 pb-3">

                    <?php do_action( 'woocommerce_checkout_before_order_review' ); ?>

                    <div id="order_review" class="woocommerce-checkout-review-order">
                        <?php do_action( 'woocommerce_checkout_order_review' ); ?>
                    </div>

                    <?php do_action( 'woocommerce_checkout_after_order_review' ); ?>
                </div>

            </div>
    </form>

    <?php do_action( 'woocommerce_after_checkout_form', $checkout ); ?>
</div>


<script type="text/javascript">
    jQuery(document).ready(function () {
        jQuery('#billing_first_name').attr('placeholder', 'First name').attr('tabindex', '888');
        jQuery('#billing_last_name').attr('placeholder', 'Last name').attr('tabindex', '889');
        jQuery('#billing_phone').attr('placeholder', 'Phone').attr('tabindex', '890');
        jQuery('#billing_email').attr('placeholder', 'Email address').attr('tabindex', '891');
        jQuery('#billing_address_1').attr('tabindex', '892');
        jQuery('#billing_address_2').attr('tabindex', '893');
        jQuery('#billing_city').attr('placeholder', 'City').attr('tabindex', '894');
        jQuery('#billing_state').attr('placeholder', 'State').attr('tabindex', '895');
        jQuery('#billing_postcode').attr('placeholder', 'Postcode').attr('tabindex', '896');
        jQuery('#order_comments').attr('tabindex', '897');

        jQuery('label[for="billing_first_name"]').hide();
        jQuery('label[for="billing_last_name"]').hide();
        jQuery('label[for="billing_address_1"]').hide();
        jQuery('label[for="billing_phone"]').hide();
        jQuery('label[for="billing_email"]').hide();
        jQuery('label[for="order_comments"]').hide();
        jQuery('label[for="billing_city"]').hide();
        jQuery('label[for="billing_state"]').hide();
        jQuery('label[for="billing_postcode"]').hide();
    });
</script>
