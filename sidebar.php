<?php
    /**
     * Created by PhpStorm.
     * User: daviddiomede
     * Date: 7/29/18
     * Time: 2:20 PM
     */
?>
<div class="col-md-4">

    <div id="sidebar" role="complementary">

        <?php if ( is_active_sidebar( 'sidebar' ) ) {
            dynamic_sidebar( 'sidebar' );
        } ?>

    </div>

</div> <!-- .col-md-4 -->